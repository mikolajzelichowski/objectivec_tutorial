//
//  AppDelegate.h
//  TutorialObjectiveC
//
//  Created by Mikolaj Zelichowski on 11/03/2021.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

