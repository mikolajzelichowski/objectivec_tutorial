//
//  main.m
//  TutorialObjectiveC
//
//  Created by Mikolaj Zelichowski on 11/03/2021.
//
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.

        appDelegateClassName = NSStringFromClass([AppDelegate class]);
//        appDelegateClassName = NSStringFromClass([SceneDelegate class]);
    }
    
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
